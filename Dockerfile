FROM busybox 
RUN   wget https://sourceforge.net/projects/jboss/files/JBoss/JBoss-4.0.5.GA/jboss-4.0.5.GA.zip/download -O /tmp/jboss-4.0.5.GA.zip \
    ; unzip -d /opt/ /tmp/jboss-4.0.5.GA.zip 

FROM   openjdk:7-jdk-alpine
ARG PROFILE
RUN addgroup -g 1000 jboss \
; adduser -u 1000 -D -G jboss jboss 

COPY --from=0 /opt/jboss-4.0.5.GA/ /opt/jboss/

RUN  chown -R jboss.jboss /opt/jboss 
    

USER jboss

ENTRYPOINT ["/opt/jboss/bin/run.sh", "-b","0.0.0.0","-c", "default"]
